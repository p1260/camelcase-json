const { convertPropertyNames, pascalUpperToLower, pascalLowerToUpper } = require("../index");

test('camelcase json', () => {
    const _json = {"RetornoId": 0, "Mensaje": "probar"};
    const _jsonlower = {"retorno_id": 0, "mensaje": "probar"};

    expect(convertPropertyNames(_json, pascalUpperToLower, convertPropertyNames)).toEqual(_jsonlower);
    expect(convertPropertyNames(_jsonlower, pascalLowerToUpper, convertPropertyNames)).toEqual(_json);
});