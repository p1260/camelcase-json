module.exports = {
    convertPropertyNames: function (obj, converterFn, convertNames) {
      var r,
        value,
        t = Object.prototype.toString.apply(obj);
  
      if (t == "[object Object]") {
        r = {};
        for (var propname in obj) {
          value = obj[propname];
          r[converterFn(propname)] = convertNames(
            value,
            converterFn,
            convertNames
          );
        }
        return r;
      } else if (t == "[object Array]") {
        r = [];
        for (var i = 0, L = obj.length; i < L; ++i) {
          value = obj[i];
          r[i] = convertNames(value, converterFn, convertNames);
        }
        return r;
      }
      return obj;
    },
  
    pascalUpperToLower: function (propname) {
      if (/([A-Z])/.test(propname)) {
        return propname
          .replace(/([A-Z])/g, "_$1")
          .toLowerCase()
          .substr(1);
      } else {
        return propname;
      }
    },
  
    pascalLowerToUpper: function (propname) {
      propname = propname.replace(
        /([a-z])_([a-z])/g,
        function (match, first, second) {
          if (second) {
            match = first + match.replace(/_/, "").substr(1).toUpperCase();
          }
  
          return match;
        }
      );
  
      return propname.charAt(0).toUpperCase() + propname.slice(1);
    }
};
  